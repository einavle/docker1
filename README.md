Objective: Small demonstration of CI/CD cycle for Dockerized product.
---------------------------------------------------------------------
1. Browse to http://ec2-18-224-138-192.us-east-2.compute.amazonaws.com/
   this is our 'qa/production' site.
2. Clone this git project (docker1) and edit the file app1/src/main/webapp/index.jsp - change the headline title. (alternative shortcut - edit the file in the online editor)
3. Commit and push to origin master with the credential: mayal, maya080409 (or just 'commit changes' if working with online-editor)
4. Now - login to jenkins in http://ec2-18-219-171-230.us-east-2.compute.amazonaws.com:8080.
   use the same credential (mayal, maya080409) and wait until your pipeline that triggered by your commit will end (Project docker1-auto)
6. Go back to step 1  - the site has automatically upgraded with the new commit version.

what behind the hook:
----------------------

gitlab triggers a jenkins pipeline -->
the jenkins job build and push a docker image to dockerhub (see the docker module pom.xml)
the last step is to invoke chef-client that in it's turn pull the suitable image from dockerHub and instantiate it on a production host.

if you wish to have a look at the chef cookbook code - it's in the second git project  - docker_cookbook.

platform services that takes part
---------------------------------

|platform         | service                  |
|-----------------|--------------------------|
|GitLab           |gitlab account            |
|ec2 amazon linux2|app production site       |
|ec2 amazon linux2| jenkins server           |
|ec2 amazon linux2|chef-server               |
|ec2 amazon linux2|chef-DK                   |
| hub.docker.com  |docker registry account   |
