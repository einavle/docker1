def tag = "einavl/webapp:app1-${project.version}-${build.number}"
println "going to build in ${project.build.directory}/docker/. the version ${tag}"
def dockerProcess = "sudo docker build ${project.build.directory}/docker/. -t ${tag}".execute()


dockerProcess.waitFor()

dockerProcess.text.eachLine {
    println it
}

println "docker build finished with exit code: " + dockerProcess.exitValue()


if(dockerProcess.exitValue() != 0) {
    throw new RuntimeException("docker build failed with exist code " + dockerProcess.exitValue())
}