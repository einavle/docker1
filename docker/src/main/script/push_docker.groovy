def tag = "einavl/webapp:app1-${project.version}-${build.number}"
def push_command = "docker push ${tag}"
def dockerProcess = push_command.execute()
dockerProcess.waitFor()

dockerProcess.text.eachLine {
    println it
}

println "docker push finished with exit code: " + dockerProcess.exitValue()


if(dockerProcess.exitValue() != 0) {
    throw new RuntimeException("docker push failed with exist code " + dockerProcess.exitValue())
}
